/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class OfficeTable extends CustomTable{
    
    public static final boolean MINI_TABLE = true;
    public static final boolean NORMAL_TABLE = false;
    
    public OfficeTable(boolean isMini) {
        if (isMini)
            this.customHeaders = new String[] {"Office Id", "Address", "Manager"};
        else
            this.customHeaders = new String[] {"Office Id", "Address", "Phone", "Fax", "Manager"};
        this.isMini = isMini;
        updateHeaders();
    }
}
