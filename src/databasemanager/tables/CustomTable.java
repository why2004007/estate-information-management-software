/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class CustomTable extends JTable{
    
    protected String customHeaders[] = {};
    protected Object data[][] = {{}};
    protected CustomTableListener customTableListener;
    protected boolean isMini = false;
    
    public CustomTable() {
        super();
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    CustomTable customTable = (CustomTable) mouseEvent.getSource();
                    if (customTable.getSelectedRowCount() == 1) 
                        onMouseDoubleClickedTable(customTable.getSelectedRow());
                }
            }
            });
    }    
    
    public void onMouseDoubleClickedTable(int rowIndex) {
        if (!isMini && customTableListener != null) {
            customTableListener.onMouseDoubleClickedTable(rowIndex);
        }
    }
    
    public void addCustomTableListener(CustomTableListener customTableListener) {
        this.customTableListener = customTableListener;
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
    
    public void updateHeaders() {
        this.setModel(new DefaultTableModel(data, customHeaders));
    }
    
    public void emptyContent() {
        DefaultTableModel dtm = (DefaultTableModel) this.getModel();
        if (dtm.getRowCount() > 0) {
            for (int i = dtm.getRowCount() - 1; i >= 0 ; i --) {
                dtm.removeRow(i);
            }
        }
    }
    
    public interface CustomTableListener {
        void onMouseDoubleClickedTable(int rowIndex);
    }
}
