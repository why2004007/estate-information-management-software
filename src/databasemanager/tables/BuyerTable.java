/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class BuyerTable extends CustomTable {
    
    public static final boolean MINI_TABLE = true;
    public static final boolean NORMAL_TABLE = false;
    
    public BuyerTable(boolean isMini) {
        if (isMini)
            this.customHeaders = new String[] {"Buyer Id", "Name", "Mobile Number"};
        else
            this.customHeaders = new String[] {"Buyer Id", "Name", "Phone Number", 
                "Mobile Number", "Email", "Address"};
        this.isMini = isMini;
        updateHeaders();
    }
}
