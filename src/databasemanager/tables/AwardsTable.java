/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class AwardsTable extends CustomTable {
    
    public AwardsTable() {
        this.customHeaders = new String[] {"Award Id", "Staff Id", "Date", "Amount", "Number of Sales"};
        updateHeaders();
    }
}
