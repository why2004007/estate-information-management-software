/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

/**
 *
 * @author Daniel
 */
public class SalesTable extends CustomTable {
    public SalesTable() {
        this.customHeaders = new String[] {"Sale Id", "Property Id", "Buyer Id",
            "Staff Id", "Date", "Sale Type", "Reserved Price", "Selling Price", "Deposit", 
            "Balance"};
        updateHeaders();
    }
}
