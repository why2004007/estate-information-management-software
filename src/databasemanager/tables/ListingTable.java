/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class ListingTable extends CustomTable {
    public ListingTable() {
        this.customHeaders = new String[] {"Listing Id", "Staff Id", "Property Id", "Date"};
        updateHeaders();
    }
}
