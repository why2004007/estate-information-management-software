/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.tables;

/**
 *
 * @author Daniel
 */
public class PropertiesTable extends CustomTable {
    
    public static final boolean MINI_TABLE = true;
    public static final boolean NORMAL_TABLE = false;
    
    public PropertiesTable(boolean isMini) {
        if (isMini)
            this.customHeaders = new String[] {"Property Id", "Address", "Suburb"};
        else
            this.customHeaders = new String[] {"Property Id", "Address", "Postcode",
                "Suburb", "Construction Year", "Bedrooms", "Carspaces",
                "Bathrooms"};
        this.isMini = isMini;
        updateHeaders();
    }
}
