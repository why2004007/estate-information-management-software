/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Buyer;
import databasemanager.MainFrame;
import databasemanager.tables.CustomTable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class BuyerTableManager extends TableManager {
    
    public BuyerTableManager() {
        this.conn = MainFrame.getConnectionA();
        this.selectAllSQLStatement = "SELECT BUYER_ID, TRIM(FNAME) || ' ' || TRIM(LNAME) AS \"Name\","
                + " PHONE, MOBILE, EMAIL, ADDRESS FROM BUYER";
    }
    
    public void viewAllMini(CustomTable customTable) {
        String sql = "SELECT BUYER_ID, TRIM(FNAME) || ' ' || TRIM(LNAME) AS \"Name\", MOBILE FROM BUYER";
        try {
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(sql);
            ResultSetMetaData rsetMeta = rset.getMetaData();
            int numberOfColumns = rsetMeta.getColumnCount();
            // Empty contents of the table.
            customTable.emptyContent();
            while (rset.next()) {
                String rowData[] = new String[numberOfColumns];
                for (int i = 0; i < rowData.length; i ++) {
                    rowData[i] = rset.getObject(i + 1).toString().trim();
                }
                DefaultTableModel dtm = (DefaultTableModel) customTable.getModel();
                dtm.addRow(rowData);
                //this.officeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean update(String buyerId, String firstName, String lastName, 
            String phone, String mobile, String email, String address) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE BUYER SET ");
	builder.append("FNAME = '").append(firstName.toUpperCase()).append("',");
        builder.append("LNAME = '").append(lastName.toUpperCase()).append("',");
        if (phone != null)
            builder.append("PHONE = '").append(phone).append("',");
        builder.append("MOBILE = '").append(mobile).append("',");
        builder.append("EMAIL = '").append(email.toUpperCase()).append("',");
        builder.append("ADDRESS = '").append(address.toUpperCase()).append("' ");
        builder.append("WHERE BUYER_ID = ").append(buyerId);
        this.updateSQLStatement = builder.toString();
        return update();
    }
    
    public boolean insert(String firstName, String lastName, 
            String phone, String mobile, String email, String address) {
        StringBuilder builder = new StringBuilder();
        if (phone != null)
            builder.append("INSERT INTO BUYER (FNAME, LNAME, PHONE, MOBILE, EMAIL, ADDRESS) VALUES (");
        else
            builder.append("INSERT INTO BUYER (FNAME, LNAME, MOBILE, EMAIL, ADDRESS) VALUES (");
        builder.append("'").append(firstName.toUpperCase()).append("',");
        builder.append("'").append(lastName.toUpperCase()).append("',");
        if (phone != null)
            builder.append("'").append(phone).append("',");
        builder.append("'").append(mobile).append("',");
        builder.append("'").append(email.toUpperCase()).append("',");
        builder.append("'").append(address.toUpperCase()).append("')");
        this.insertSQLStatment = builder.toString();
        return insert();
    }
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM BUYER WHERE BUYER_ID = " + Id;
        return remove();
    }
    
    @Override
    public Buyer getEntity(String Id) {
        Buyer buyer = new Buyer();
        this.selectAllSQLStatement = "SELECT * FROM BUYER WHERE BUYER_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                buyer.setBuyerId(rset.getString(1).trim());
                buyer.setFirstName(rset.getString(2).trim());
                buyer.setLastName(rset.getString(3).trim());
                Object phone = rset.getObject(4);
                if (phone != null)
                    buyer.setPhone(phone.toString().trim());
                else
                    buyer.setPhone("");
                buyer.setMobile(rset.getString(5).trim());
                buyer.setEmail(rset.getString(6).trim());
                buyer.setAddress(rset.getString(7).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return buyer;
    }
}
