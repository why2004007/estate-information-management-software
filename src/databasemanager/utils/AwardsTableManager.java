/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Awards;
import databasemanager.MainFrame;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class AwardsTableManager extends TableManager {
    
    public AwardsTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT AWARD_ID, STAFF_ID, TO_CHAR(\"date\", 'YYYY-MM-DD'), AMOUNT, SALES FROM AWARDS";
    }
    
    public boolean insert(String staffId, String date, String amount, String sales) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO AWARDS (STAFF_ID, \"date\", AMOUNT, SALES) VALUES (");
        builder.append(staffId).append(", ");
        builder.append("TO_DATE('").append(date).append("','YYYY-MM-DD'), ");
        builder.append(amount).append(", ");
        builder.append(sales).append(")");
        this.insertSQLStatment = builder.toString();
        return insert();
    }
    
    public boolean update(String awardId, String staffId, String date, String amount, String sales) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE AWARDS SET ");
        builder.append("STAFF_ID = ").append(staffId).append(", ");
        builder.append("\"date\" = TO_DATE('").append(date).append("', 'YYYY-MM-DD'), ");
        builder.append("AMOUNT = ").append(amount).append(", ");
        builder.append("SALES = '").append(sales).append(" ");
        builder.append("WHERE AWARD_ID = ").append(awardId);
        this.updateSQLStatement = builder.toString();
        return update();
    }
    
    public boolean updateByStoredProcedure(String awardId, String staffId, Date date, String amount, String sales) {
        
        try {
            CallableStatement stmt = conn.prepareCall("call UPDATEAWARDS(?,?,?,?,?)");
            stmt.setInt(1, Integer.parseInt(awardId)); 
            stmt.setInt(2, Integer.parseInt(staffId));
            stmt.setDate(3, new java.sql.Date(date.getTime()));
            stmt.setInt(4, Integer.parseInt(amount));
            stmt.setInt(5, Integer.parseInt(sales));
            stmt.executeUpdate();
            //conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM AWARDS WHERE AWARD_ID = " + Id;
        return remove();
    }
    
    @Override
    public Awards getEntity(String Id) {
        Awards awards = new Awards();
        this.selectAllSQLStatement = "SELECT AWARD_ID, STAFF_ID, TO_CHAR(\"date\", 'YYYY-MM-DD'), AMOUNT, SALES FROM AWARDS WHERE AWARD_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                awards.setAwardId(rset.getString(1).trim());
                awards.setStaffId(rset.getString(2).trim());
                awards.setDate(rset.getString(3).trim());
                awards.setAmount(rset.getString(4).trim());
                awards.setSales(rset.getString(5).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return awards;
    }
}
