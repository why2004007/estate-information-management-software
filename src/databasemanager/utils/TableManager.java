/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.tables.CustomTable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class TableManager {
    
    protected Connection conn;
    protected Statement stmt;
    protected String selectAllSQLStatement;
    protected String insertSQLStatment;
    protected String updateSQLStatement;
    protected String selectByIdSQLStatement;
    protected String removeByIdSQLStatement;
    
    public TableManager() {
        
    }
    
    public ResultSet selectById() {
        ResultSet rset = null;
        try {
            stmt = conn.createStatement();
            rset = stmt.executeQuery(selectAllSQLStatement);
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rset;
    }
    
    public void viewAll(CustomTable customTable) {
        try {
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(selectAllSQLStatement);
            ResultSetMetaData rsetMeta = rset.getMetaData();
            int numberOfColumns = rsetMeta.getColumnCount();
            // Empty contents of the table.
            customTable.emptyContent();
            while (rset.next()) {
                String rowData[] = new String[numberOfColumns];
                for (int i = 0; i < rowData.length; i ++) {
                    Object result = rset.getObject(i + 1);
                    if (result == null)
                        rowData[i] = null;
                    else
                        rowData[i] = rset.getObject(i + 1).toString().trim();
                }
                DefaultTableModel dtm = (DefaultTableModel) customTable.getModel();
                dtm.addRow(rowData);
                //this.officeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected boolean insert() {
        try {
            stmt = conn.createStatement();
            stmt.executeQuery(insertSQLStatment);
            //Auto commit is on.
            //conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    protected boolean update() {
        try {
            stmt = conn.createStatement();
            stmt.executeQuery(updateSQLStatement);
            //Auto commit is on.
            //conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    protected boolean remove() {
        try {
            stmt = conn.createStatement();
            stmt.executeQuery(removeByIdSQLStatement);
            //Auto commit is on.
            //conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public Object getEntity(String Id) {
        return new Object();
    }
}
