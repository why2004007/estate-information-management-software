/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Listing;
import databasemanager.MainFrame;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class ListingTableManager extends TableManager {
    public ListingTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT LIST_ID, STAFF_ID, PROP_ID, TO_CHAR(\"date\", 'YYYY-MM-DD') FROM LISTING";
    }
    
    public boolean insert(String staffId, String propId, String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO LISTING (STAFF_ID, PROP_ID, \"date\") VALUES (");
        builder.append(staffId).append(",");
        builder.append(propId).append(",");
        builder.append("TO_DATE('").append(date).append("','YYYY-MM-DD')");
        builder.append(")");
        this.insertSQLStatment = builder.toString();
        return insert();
    }   
    public boolean update(String listId, String staffId, String propId, String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE LISTING SET ");
        builder.append("STAFF_ID = ").append(staffId).append(",");
        builder.append("PROP_ID = ").append(propId).append(",");
        builder.append("\"date\" = TO_DATE('").append(date).append("','YYYY-MM-DD')");
        builder.append("WHERE LIST_ID = ").append(listId);
        this.updateSQLStatement = builder.toString();
        return update();
    }   
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM LISTING WHERE LIST_ID = " + Id;
        return remove();
    }
    
    @Override
    public Listing getEntity(String Id) {
        Listing listing = new Listing();
        this.selectAllSQLStatement = "SELECT LIST_ID, STAFF_ID, PROP_ID, TO_CHAR(\"date\", 'YYYY-MM-DD') FROM LISTING WHERE LIST_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                listing.setListId(rset.getString(1).trim());
                listing.setStaffId(rset.getString(2).trim());
                listing.setPropId(rset.getString(3).trim());
                listing.setDate(rset.getString(4).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listing;
    }
}
