/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Properties;
import databasemanager.MainFrame;
import databasemanager.tables.CustomTable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class PropertiesTableManager extends TableManager {
    
    public PropertiesTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT PROPERTY_ID, ADDRESS, POSTCODE, "
                + "SUBURB, CONSTRUCTION_YEAR, BEDROOMS, CARSPACES, BATHROOMS FROM PROPERTIES";
    }
    
    public void viewAllMini(CustomTable customTable) {
        String sql = "SELECT PROP_ID, ADDRESS, SUBURB FROM PROPERTIES";
        try {
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(sql);
            ResultSetMetaData rsetMeta = rset.getMetaData();
            int numberOfColumns = rsetMeta.getColumnCount();
            // Empty contents of the table.
            customTable.emptyContent();
            while (rset.next()) {
                String rowData[] = new String[numberOfColumns];
                for (int i = 0; i < rowData.length; i ++) {
                    rowData[i] = rset.getObject(i + 1).toString().trim();
                }
                DefaultTableModel dtm = (DefaultTableModel) customTable.getModel();
                dtm.addRow(rowData);
                //this.officeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean insert(String address, String postcode, String year, 
            String bedrooms, String carspaces, String bathrooms, String suburb) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO PROPERTIES (ADDRESS, POSTCODE, CONSTRUCTION_YEAR, BEDROOMS, CARSPACES, BATHROOMS, SUBURB) VALUES (");
        builder.append("'").append(address.toUpperCase()).append("',");
        builder.append("'").append(postcode).append("',");
        builder.append(year).append(",");
        builder.append(bedrooms).append(",");
        builder.append(carspaces).append(",");
        builder.append(bathrooms).append(",");
        builder.append("'").append(suburb.toUpperCase()).append("')");
        this.insertSQLStatment = builder.toString();
        return insert();
    }   
    public boolean update(String propId, String address, String postcode, 
            String year, String bedrooms, String carspaces, String bathrooms, String suburb) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE PROPERTIES SET ");
        builder.append("ADDRESS = '").append(address.toUpperCase()).append("', ");
        builder.append("POSTCODE = '").append(postcode).append("', ");
        builder.append("CONSTRUCTION_YEAR = ").append(year).append(",");
        builder.append("BEDROOMS = ").append(bedrooms).append(",");
        builder.append("CARSPACES = ").append(carspaces).append(",");
        builder.append("BATHROOMS = ").append(bathrooms).append(",");
        builder.append("SUBURB = '").append(suburb.toUpperCase()).append("'");
        builder.append("WHERE PROP_ID = ").append(propId);
        this.updateSQLStatement = builder.toString();
        return update();
    }   
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM PROPERTIES WHERE PROP_ID = " + Id;
        return remove();
    }
    
    @Override
    public Properties getEntity(String Id) {
        Properties properties = new Properties();
        this.selectAllSQLStatement = "SELECT * FROM PROPERTIES WHERE PROP_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                properties.setPropId(rset.getString(1).trim());
                properties.setAddress(rset.getString(2).trim());
                properties.setPostcode(rset.getString(3).trim());
                properties.setYear(rset.getString(4).trim());
                properties.setBedrooms(rset.getString(5).trim());
                properties.setCarspaces(rset.getString(6).trim());
                properties.setBathrooms(rset.getString(7).trim());
                properties.setSuburb(rset.getString(8).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return properties;
    }
}
