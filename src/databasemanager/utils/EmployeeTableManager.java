/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Employee;
import databasemanager.MainFrame;
import databasemanager.tables.CustomTable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class EmployeeTableManager extends TableManager {
    
    
    public EmployeeTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT STAFF_ID, TRIM(FNAME) || ' ' || TRIM(LNAME) AS \"Name\","
                + "ADDRESS, PHONE, MOBILE, EMAIL, POSITION, OFFICE_ID FROM EMPLOYEE";
    }
    
    public void viewAllMini(CustomTable customTable) {
        String sql = "SELECT STAFF_ID, TRIM(FNAME) || ' ' || TRIM(LNAME) AS \"Name\", POSITION, OFFICE_ID FROM EMPLOYEE";
        try {
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(sql);
            ResultSetMetaData rsetMeta = rset.getMetaData();
            int numberOfColumns = rsetMeta.getColumnCount();
            // Empty contents of the table.
            customTable.emptyContent();
            while (rset.next()) {
                String rowData[] = new String[numberOfColumns];
                for (int i = 0; i < rowData.length; i ++) {
                    rowData[i] = rset.getObject(i + 1).toString().trim();
                }
                DefaultTableModel dtm = (DefaultTableModel) customTable.getModel();
                dtm.addRow(rowData);
                //this.officeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean insert(String firstName, String lastName,
        String address, String phone, String mobile,
        String email, String position, String officeId) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO EMPLOYEE(FNAME, LNAME, ADDRESS, ");
            if (phone != null)
                builder.append("PHONE, ");
            builder.append("MOBILE, EMAIL, POSITION, OFFICE_ID) "
                    + "VALUES (");
            builder.append("'").append(firstName.toUpperCase()).append("', ");
            builder.append("'").append(lastName.toUpperCase()).append("', ");
            builder.append("'").append(address.toUpperCase()).append("', ");
            if (phone != null)
                builder.append("'").append(phone).append("', ");
            builder.append("'").append(mobile).append("', ");
            builder.append("'").append(email.toUpperCase()).append("', ");
            builder.append("'").append(position.toUpperCase()).append("', ");
            builder.append(officeId);
            builder.append(")");
        this.insertSQLStatment = builder.toString();
        return insert();
    }
    public boolean update(String staffId, String firstName, String lastName,
        String address, String phone, String mobile,
        String email, String position, String officeId) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE EMPLOYEE SET ");
        builder.append("FNAME = ").append("'").append(firstName.toUpperCase()).append("', ");
        builder.append("LNAME = ").append("'").append(lastName.toUpperCase()).append("', ");
        builder.append("ADDRESS = ").append("'").append(address.toUpperCase()).append("', ");
        if (phone != null)
            builder.append("PHONE = ").append("'").append(phone).append("', ");
        builder.append("MOBILE = ").append("'").append(mobile).append("', ");
        builder.append("EMAIL = ").append("'").append(email.toUpperCase()).append("', ");
        builder.append("POSITION = ").append("'").append(position.toUpperCase()).append("', ");
        builder.append("OFFICE_ID = ").append(officeId);
        builder.append("WHERE STAFF_ID = ").append(staffId);
        this.updateSQLStatement = builder.toString();
        return update();
    }
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM EMPLOYEE WHERE STAFF_ID = " + Id;
        return remove();
    }
    
    @Override
    public Employee getEntity(String Id) {
        Employee employee = new Employee();
        this.selectAllSQLStatement = "SELECT * FROM EMPLOYEE WHERE STAFF_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                employee.setStaffId(rset.getString(1).trim());
                employee.setFirstName(rset.getString(2).trim());
                employee.setLastName(rset.getString(3).trim());
                employee.setAddress(rset.getString(4).trim());
                Object phone = rset.getObject(5);
                if (phone != null)
                    employee.setPhone(phone.toString().trim());
                else
                    employee.setPhone("");
                employee.setMobile(rset.getString(6).trim());
                employee.setEmail(rset.getString(7).trim());
                employee.setPosition(rset.getString(8).trim());
                employee.setOfficeId(rset.getString(9).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return employee;
    }
}
