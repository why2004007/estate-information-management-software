/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Office;
import databasemanager.MainFrame;
import databasemanager.tables.CustomTable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class OfficeTableManager extends TableManager {
    
    public OfficeTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT * FROM OFFICE";
    }
    
    public void viewAllMini(CustomTable customTable) {
        String sql = "SELECT OFFICE_ID, ADDRESS, STAFF_ID FROM OFFICE";
        try {
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(sql);
            ResultSetMetaData rsetMeta = rset.getMetaData();
            int numberOfColumns = rsetMeta.getColumnCount();
            // Empty contents of the table.
            customTable.emptyContent();
            while (rset.next()) {
                String rowData[] = new String[numberOfColumns];
                for (int i = 0; i < rowData.length; i ++) {
                    rowData[i] = rset.getObject(i + 1).toString().trim();
                }
                DefaultTableModel dtm = (DefaultTableModel) customTable.getModel();
                dtm.addRow(rowData);
                //this.officeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean insert(String address, String phone, String fax, String staffId) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO OFFICE (ADDRESS, PHONE, ");
        if (fax != null)
            builder.append("FAX, ");
        if (staffId != null)
            builder.append("STAFF_ID");
        builder.append(") VALUES (");
        builder.append("'").append(address.toUpperCase()).append("',");
        builder.append("'").append(phone).append("',");
        if (fax != null)
            builder.append("'").append(fax).append("',");
        if (staffId != null)
            builder.append(staffId);
        builder.append(")");
        this.insertSQLStatment = builder.toString();
        return insert();
    }   
    public boolean update(String officeId, String address, String phone, String fax, String staffId) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE OFFICE SET ");
        builder.append("ADDRESS = '").append(address.toUpperCase()).append("',");
        builder.append("PHONE = '").append(phone).append("'");
        if (fax != null)
            builder.append(",FAX = '").append(fax).append("'");
        if (staffId != null)    
            builder.append(",STAFF_ID = ").append(staffId);
        builder.append(" WHERE OFFICE_ID = ").append(officeId);
        this.updateSQLStatement = builder.toString();
        return update();
    }   
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM OFFICE WHERE OFFICE_ID = " + Id;
        return remove();
    }
    
    @Override
    public Office getEntity(String Id) {
        Office office = new Office();
        this.selectAllSQLStatement = "SELECT * FROM OFFICE WHERE OFFICE_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                office.setOfficeId(rset.getString(1).trim());
                office.setAddress(rset.getString(2).trim());
                office.setPhone(rset.getString(3).trim());
                Object fax = rset.getObject(4);
                if (fax != null)
                    office.setFax(fax.toString().trim());
                else
                    office.setFax("");
                Object staffId = rset.getObject(5);
                if (staffId != null)
                    office.setStaffId(staffId.toString().trim());
                else
                    office.setStaffId("");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return office;
    }
}
