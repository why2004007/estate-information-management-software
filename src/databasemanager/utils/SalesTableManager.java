/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.utils;

import databasemanager.Entities.Sales;
import databasemanager.MainFrame;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class SalesTableManager extends TableManager {
    public SalesTableManager() {
        this.conn = MainFrame.getConnectionB();
        this.selectAllSQLStatement = "SELECT SALE_ID, PROP_ID, BUYER_ID, STAFF_ID, TO_CHAR(\"date\", 'YYYY-MM-DD'), RESERVED, SELLING, DEPOSIT, BALANCE, TYPE FROM SALES";
    }
    public boolean insert(String propId, String buyerId, String staffId, 
            String date, String reserved, String selling, String deposit, 
            String balance, String type) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO SALES (PROP_ID, BUYER_ID, STAFF_ID, \"date\", RESERVED, SELLING, DEPOSIT, BALANCE, TYPE) VALUES (");
        builder.append(propId).append(",");
        builder.append(buyerId).append(",");
        builder.append(staffId).append(",");
        builder.append("TO_DATE('").append(date).append("','YYYY-MM-DD'),");
        builder.append(reserved).append(",");
        builder.append(selling).append(",");
        builder.append(deposit).append(",");
        builder.append(balance).append(",");
        builder.append("'").append(type).append("')");
        this.insertSQLStatment = builder.toString();
        return insert();
    }   
    public boolean update(String saleId, String propId, String buyerId, 
            String staffId, String date, String reserved, String selling, 
            String deposit, String balance, String type) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE SALES SET ");
        builder.append("PROP_ID = ").append(propId).append(",");
        builder.append("BUYER_ID = ").append(builder).append(",");
        builder.append("STAFF_ID = ").append(staffId).append(",");
        builder.append("\"date\" = TO_DATE('").append(date).append("','YYYY-MM-DD'),");
        builder.append("RESERVED = ").append(reserved).append(",");
        builder.append("SELLING = ").append(selling).append(",");
        builder.append("DEPOSIT = ").append(deposit).append(",");
        builder.append("BALANCE = ").append(balance).append(",");
        builder.append("TYPE = '").append(type).append("'");
        builder.append("WHERE SALE_ID = ").append(saleId);
        this.updateSQLStatement = builder.toString();
        return update();
    }   
    
    public boolean remove(String Id) {
        this.removeByIdSQLStatement = "DELETE FROM SALES WHERE SALE_ID = " + Id;
        return remove();
    }
    
    @Override
    public Sales getEntity(String Id) {
        Sales sales = new Sales();
        this.selectAllSQLStatement = "SELECT SALE_ID, PROP_ID, BUYER_ID, STAFF_ID, TO_CHAR(\"date\", 'YYYY-MM-DD'), RESERVED, SELLING, DEPOSIT, BALANCE, TYPE FROM SALES WHERE SALE_ID = " + Id;
        ResultSet rset = selectById();
        try {
            if (rset.next()) {
                sales.setSaleId(rset.getString(1).trim());
                sales.setPropId(rset.getString(2).trim());
                sales.setBuyerId(rset.getString(3).trim());
                sales.setStaffId(rset.getString(4).trim());
                sales.setDate(rset.getString(5).trim());
                sales.setReserved(rset.getString(6).trim());
                sales.setSelling(rset.getString(7).trim());
                sales.setDeposit(rset.getString(8).trim());
                sales.setBalance(rset.getString(9).trim());
                sales.setType(rset.getString(10).trim());
            }
        } catch (SQLException ex) {
            Logger.getLogger(AwardsTableManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sales;
    }
}
