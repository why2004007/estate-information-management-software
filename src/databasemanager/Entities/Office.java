/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.Entities;

/**
 *
 * @author Daniel
 */
public class Office {
    private String officeId;
    private String address;
    private String phone;
    private String fax;
    private String staffId;

    public Office() {
    }

    public Office(String officeId, String address, String phone, String fax, String staffId) {
        this.officeId = officeId;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.staffId = staffId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
 
}
