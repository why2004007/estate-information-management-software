/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.Entities;

/**
 *
 * @author Daniel
 */
public class Listing {
    private String listId;
    private String staffId;
    private String propId;
    private String date;

    public Listing() {
    }

    public Listing(String listId, String staffId, String propId, String date) {
        this.listId = listId;
        this.staffId = staffId;
        this.propId = propId;
        this.date = date;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getPropId() {
        return propId;
    }

    public void setPropId(String propId) {
        this.propId = propId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
