/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.Entities;

/**
 *
 * @author Daniel
 */
public class Sales {
    private String saleId;
    private String propId;
    private String buyerId;
    private String staffId;
    private String date;
    private String reserved;
    private String selling;
    private String deposit;
    private String balance;
    private String type;

    public Sales() {
    }

    public Sales(String saleId, String propId, String buyerId, String staffId, String date, String reserved, String selling, String deposit, String balance, String type) {
        this.saleId = saleId;
        this.propId = propId;
        this.buyerId = buyerId;
        this.staffId = staffId;
        this.date = date;
        this.reserved = reserved;
        this.selling = selling;
        this.deposit = deposit;
        this.balance = balance;
        this.type = type;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getPropId() {
        return propId;
    }

    public void setPropId(String propId) {
        this.propId = propId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getSelling() {
        return selling;
    }

    public void setSelling(String selling) {
        this.selling = selling;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
