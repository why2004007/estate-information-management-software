/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.Entities;

/**
 *
 * @author Daniel
 */
public class Properties {
    private String propId;
    private String address;
    private String postcode;
    private String year;
    private String bedrooms;
    private String carspaces;
    private String bathrooms;
    private String suburb;

    public Properties() {
    }

    public Properties(String propId, String address, String postcode, String year, String bedrooms, String carspaces, String bathrooms, String suburb) {
        this.propId = propId;
        this.address = address;
        this.postcode = postcode;
        this.year = year;
        this.bedrooms = bedrooms;
        this.carspaces = carspaces;
        this.bathrooms = bathrooms;
        this.suburb = suburb;
    }

    public String getPropId() {
        return propId;
    }

    public void setPropId(String propId) {
        this.propId = propId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getCarspaces() {
        return carspaces;
    }

    public void setCarspaces(String carspaces) {
        this.carspaces = carspaces;
    }

    public String getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }
    

}
