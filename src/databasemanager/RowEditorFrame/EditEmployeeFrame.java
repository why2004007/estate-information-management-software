/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.RowEditorFrame;

import databasemanager.Entities.Employee;
import databasemanager.IdSelectorFrame.SelectOfficeIdFrame;
import databasemanager.Panels.CustomPanel;
import databasemanager.utils.EmployeeTableManager;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Daniel
 */
public class EditEmployeeFrame extends javax.swing.JFrame {
    
    public static final int EDIT_EMPLOYEE = 0;
    public static final int INSERT_EMPLOYEE = 1;
    public static final int NO_STUFF_ID = 0;
    private String dropDownList[] = new String[] {"Choose position", "Agent", "Admin", "Marketing"};
    private boolean isUpdate = false;
    private Employee employee;
    private CustomPanel parentWindow;
    /**
     * Creates new form EditEmployeeFrame
     */
    
    public EditEmployeeFrame(CustomPanel parentWindow) {
        initComponents();
        this.parentWindow = parentWindow;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        this.setTitle("New Employee");
    }
    
    public EditEmployeeFrame(CustomPanel parentWindow,Employee employee) {
        initComponents();
        this.parentWindow = parentWindow;
        isUpdate = true;
        this.employee = employee;
        updateContents();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    private void updateContents() {
        this.firstNameField.setText(employee.getFirstName());
        this.firstNameField.setCaretPosition(0);
        this.lastNameField.setText(employee.getLastName());
        this.lastNameField.setCaretPosition(0);
        this.addressField.setText(employee.getAddress());
        this.addressField.setCaretPosition(0);
        this.phoneField.setText(employee.getPhone());
        this.phoneField.setCaretPosition(0);
        this.mobileField.setText(employee.getMobile());
        this.mobileField.setCaretPosition(0);
        this.emailField.setText(employee.getEmail());
        this.emailField.setCaretPosition(0);
        if (employee.getPosition().equals("AGENT"))
            this.positionBox.setSelectedIndex(1);
        if (employee.getPosition().equals("ADMIN"))
            this.positionBox.setSelectedIndex(2);
        if (employee.getPosition().equals("MARKETING"))
            this.positionBox.setSelectedIndex(3);
        this.officeIdField.setText(employee.getOfficeId());
        this.officeIdField.setCaretPosition(0);
        
    }
    
    private boolean checkForm() {
        boolean result = true;
        StringBuilder builder = new StringBuilder();
        builder.append("You must fill in");
        if (this.firstNameField.getText().length() == 0) {
            builder.append(" First name,");
            result = false;
        }
        if (this.lastNameField.getText().length() == 0) {
            builder.append(" Last name,");
            result = false;
        }
        if (this.addressField.getText().length() == 0) {
            builder.append(" Address,");
            result = false;
        }
        if (this.mobileField.getText().length() == 0) {
            builder.append(" Mobile,");
            result = false;
        }
        if (this.emailField.getText().length() == 0) {
            builder.append(" Email,");
            result = false;
        }
        if (this.positionBox.getSelectedIndex() == 0) {
            builder.append(" Position,");
            result = false;
        }
        if (this.officeIdField.getText().length() == 0) {
            builder.append(" Office Id,");
            result = false;
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append(" to submit.");
        if (! result) {
            JOptionPane.showMessageDialog(this, builder.toString(), "Error", JOptionPane.ERROR_MESSAGE); 
            return result;
        }
        // Split check
        builder = new StringBuilder();
        
        if (this.phoneField.getText().length() != 0 && this.phoneField.getText().length() != 10) {
            builder.append("Phone number format error.");
            result = false;
        }
        if (this.mobileField.getText().length() != 10) {
            builder.append("Mobile number format error.");
            result = false;
        }
        if (!this.emailField.getText().matches("[a-zA-Z\\-\\_0-9]*@[a-zA-Z0-9]*\\.[a-zA-Z]*")) {
            builder.append("Email format error.");
            result = false;
        }
        try {
            Integer.parseInt(this.officeIdField.getText());
        } catch (NumberFormatException e) {
            builder.append("Office id incorrect.\n");
            result = false;
        }
        if (!result)
            JOptionPane.showMessageDialog(this, builder.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        return result;
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel2 = new javax.swing.JLabel();
        firstNameField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lastNameField = new javax.swing.JTextField();
        phoneField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        mobileField = new javax.swing.JTextField();
        addressField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        officeIdField = new javax.swing.JTextField();
        cancelButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        positionBox = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("First Name:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 15, 0, 0);
        getContentPane().add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        getContentPane().add(firstNameField, gridBagConstraints);

        jLabel3.setText("Last Name:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        getContentPane().add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 15);
        getContentPane().add(lastNameField, gridBagConstraints);

        phoneField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(phoneField, gridBagConstraints);

        jLabel4.setText("Phone:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel4, gridBagConstraints);

        jLabel5.setText("Mobile:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(jLabel5, gridBagConstraints);

        mobileField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mobileFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 15);
        getContentPane().add(mobileField, gridBagConstraints);

        addressField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addressFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(addressField, gridBagConstraints);

        jLabel6.setText("Address:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel6, gridBagConstraints);

        jLabel7.setText("Email:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(emailField, gridBagConstraints);

        jLabel8.setText("Position");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel8, gridBagConstraints);

        jLabel9.setText("Office Id");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 10, 0);
        getContentPane().add(jLabel9, gridBagConstraints);

        officeIdField.setEditable(false);
        officeIdField.setText("Click to select office");
        officeIdField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                officeIdFieldMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        getContentPane().add(officeIdField, gridBagConstraints);

        cancelButton.setText("Cancel");
        cancelButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelButtonMouseClicked(evt);
            }
        });
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 15);
        getContentPane().add(cancelButton, gridBagConstraints);

        saveButton.setText("Save");
        saveButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveButtonMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 15);
        getContentPane().add(saveButton, gridBagConstraints);

        positionBox.setModel(new javax.swing.DefaultComboBoxModel(dropDownList));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(positionBox, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addressFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addressFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addressFieldActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveButtonMouseClicked
        EmployeeTableManager etm = new EmployeeTableManager();
        if (checkForm()) {
            String phone;
            if (phoneField.getText().length() != 0)
                phone = phoneField.getText();
            else
                phone = null;
            if (!isUpdate) {
                etm.insert(firstNameField.getText(), lastNameField.getText(), 
                        addressField.getText(), phone, mobileField.getText(),
                        emailField.getText(), (String) positionBox.getSelectedItem().toString().toUpperCase(),
                        officeIdField.getText());
            } else {
                etm.update(employee.getStaffId(), firstNameField.getText(), lastNameField.getText(), 
                        addressField.getText(), phone, mobileField.getText(),
                        emailField.getText(), (String) positionBox.getSelectedItem().toString().toUpperCase(),
                        officeIdField.getText());
            }
            parentWindow.updateTableData();
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }//GEN-LAST:event_saveButtonMouseClicked

    private void officeIdFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_officeIdFieldMouseClicked
        SelectOfficeIdFrame selectOfficeIdFrame = new SelectOfficeIdFrame(officeIdField);
        selectOfficeIdFrame.setVisible(true);
    }//GEN-LAST:event_officeIdFieldMouseClicked

    private void cancelButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelButtonMouseClicked
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_cancelButtonMouseClicked

    private void mobileFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mobileFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mobileFieldActionPerformed

    private void phoneFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_phoneFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField addressField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField emailField;
    private javax.swing.JTextField firstNameField;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField lastNameField;
    private javax.swing.JTextField mobileField;
    private javax.swing.JTextField officeIdField;
    private javax.swing.JTextField phoneField;
    private javax.swing.JComboBox positionBox;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables
}
