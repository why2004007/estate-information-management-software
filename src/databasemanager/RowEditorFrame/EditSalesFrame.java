/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemanager.RowEditorFrame;

import databasemanager.Entities.Sales;
import databasemanager.IdSelectorFrame.SelectBuyerIdFrame;
import databasemanager.IdSelectorFrame.SelectPropertyIdFrame;
import databasemanager.IdSelectorFrame.SelectStaffIdFrame;
import databasemanager.Panels.CustomPanel;
import databasemanager.utils.SalesTableManager;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Daniel
 */
public class EditSalesFrame extends javax.swing.JFrame {
    
    private String dropDownList[] = new String[] {"Auction", "Private"};
    private boolean isUpdate = false;
    private Sales sales;
    private CustomPanel parentWindow;
    /**
     * Creates new form EditSalesFrame
     */
    public EditSalesFrame(CustomPanel parentWindow) {
        initComponents();
        this.parentWindow = parentWindow;
        this.datePicker.setFormats("yyyy-MM-dd");
        this.datePicker.getEditor().setEditable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        this.setTitle("New Sale");
    }
    
    public EditSalesFrame(CustomPanel parentWindow,Sales sales) {
        initComponents();
        this.parentWindow = parentWindow;
        this.isUpdate = true;
        this.sales = sales;
        updateContents();
        this.datePicker.setFormats("yyyy-MM-dd");
        this.datePicker.getEditor().setEditable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        this.setTitle("New Sale");
    }
    
    private void updateContents() {
        this.propertyIdField.setText(sales.getPropId());
        this.buyerIdField.setText(sales.getBuyerId());
        this.staffIdField.setText(sales.getStaffId());
        //this.datePicker.getEditor().setText(sales.getDate());
        try {
            //this.datePicker.getEditor().setText(awards.getDate());
            this.datePicker.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(sales.getDate()));
        } catch (ParseException ex) {
            Logger.getLogger(EditAwardsFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.reservedField.setText(sales.getReserved());
        this.sellingField.setText(sales.getSelling());
        this.depositField.setText(sales.getDeposit());
        this.balanceField.setText(sales.getBalance());
        if (sales.getType().equals("AUCTION"))
            this.sellTypeBox.setSelectedIndex(0);
        else
            this.sellTypeBox.setSelectedIndex(1);
    }
    
    private boolean checkForm() {
        boolean result = true;
        StringBuilder builder = new StringBuilder();
        builder.append("You must fill in");
        if (this.staffIdField.getText().equals("Click to select")) {
            builder.append(" Employee,");
            result = false;
        }
        if (this.datePicker.getEditor().getText().length() == 0) {
            builder.append(" Date,");
            result = false;
        }
        if (this.propertyIdField.getText().length() == 0) {
            builder.append(" Amount,");
            result = false;
        }
        if (this.buyerIdField.getText().length() == 0) {
            builder.append(" Sales,");
            result = false;
        }
        if (this.reservedField.getText().length() == 0) {
            builder.append(" Amount,");
            result = false;
        }
        if (this.sellingField.getText().length() == 0) {
            builder.append(" Sales,");
            result = false;
        }
        if (this.depositField.getText().length() == 0) {
            builder.append(" Amount,");
            result = false;
        }
        if (this.balanceField.getText().length() == 0) {
            builder.append(" Sales,");
            result = false;
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append(" to submit.");
        if (!result) {
            JOptionPane.showMessageDialog(this, builder.toString(), "Error", JOptionPane.ERROR_MESSAGE); 
            return result;
        }
        // Split check
        builder = new StringBuilder();
        if (this.datePicker.getDate().after(new Date())) {
            builder.append("You must choose a time which is before or the same as today.");
            result = false;
        }
        if (!result)
            JOptionPane.showMessageDialog(this, builder.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        return result;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        propertyIdField = new javax.swing.JTextField();
        staffIdField = new javax.swing.JTextField();
        buyerIdField = new javax.swing.JTextField();
        datePicker = new org.jdesktop.swingx.JXDatePicker();
        sellTypeBox = new javax.swing.JComboBox();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        balanceField = new javax.swing.JTextField();
        sellingField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        depositField = new javax.swing.JTextField();
        reservedField = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Property:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 15, 0, 5);
        getContentPane().add(jLabel1, gridBagConstraints);

        jLabel2.setText("Staff:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 0, 0);
        getContentPane().add(jLabel2, gridBagConstraints);

        jLabel3.setText("Buyer:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 5);
        getContentPane().add(jLabel3, gridBagConstraints);

        jLabel4.setText("Date:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 5);
        getContentPane().add(jLabel4, gridBagConstraints);

        jLabel9.setText("Sell Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        getContentPane().add(jLabel9, gridBagConstraints);

        propertyIdField.setEditable(false);
        propertyIdField.setText("Click to select");
        propertyIdField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                propertyIdFieldMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        getContentPane().add(propertyIdField, gridBagConstraints);

        staffIdField.setEditable(false);
        staffIdField.setText("Click to select");
        staffIdField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                staffIdFieldMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 15);
        getContentPane().add(staffIdField, gridBagConstraints);

        buyerIdField.setEditable(false);
        buyerIdField.setText("Click to select");
        buyerIdField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buyerIdFieldMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(buyerIdField, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(datePicker, gridBagConstraints);

        sellTypeBox.setModel(new javax.swing.DefaultComboBoxModel(dropDownList));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 15);
        getContentPane().add(sellTypeBox, gridBagConstraints);

        saveButton.setText("Save");
        saveButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveButtonMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        getContentPane().add(saveButton, gridBagConstraints);

        cancelButton.setText("Cancel");
        cancelButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelButtonMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        getContentPane().add(cancelButton, gridBagConstraints);

        jLabel6.setText("Selling:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        getContentPane().add(jLabel6, gridBagConstraints);

        jLabel8.setText("Balance:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        getContentPane().add(jLabel8, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 15);
        getContentPane().add(balanceField, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 15);
        getContentPane().add(sellingField, gridBagConstraints);

        jLabel5.setText("Reserved:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel5, gridBagConstraints);

        jLabel7.setText("Deposit:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        getContentPane().add(jLabel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        getContentPane().add(depositField, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 82;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        getContentPane().add(reservedField, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 15);
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void propertyIdFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_propertyIdFieldMouseClicked
        SelectPropertyIdFrame spif = new SelectPropertyIdFrame(propertyIdField);
        spif.setVisible(true);
    }//GEN-LAST:event_propertyIdFieldMouseClicked

    private void staffIdFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_staffIdFieldMouseClicked
        SelectStaffIdFrame ssif = new SelectStaffIdFrame(staffIdField);
        ssif.setVisible(true);
    }//GEN-LAST:event_staffIdFieldMouseClicked

    private void buyerIdFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buyerIdFieldMouseClicked
        SelectBuyerIdFrame sbif = new SelectBuyerIdFrame(buyerIdField);
        sbif.setVisible(true);
    }//GEN-LAST:event_buyerIdFieldMouseClicked

    private void cancelButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelButtonMouseClicked
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_cancelButtonMouseClicked

    private void saveButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveButtonMouseClicked
        if (checkForm()) {
            SalesTableManager stm = new SalesTableManager();
            if (isUpdate) {
                stm.update(sales.getSaleId(), propertyIdField.getText(), 
                        buyerIdField.getText(), staffIdField.getText(), 
                        datePicker.getEditor().getText(), reservedField.getText(),
                        sellingField.getText(),depositField.getText(),
                        balanceField.getText(), sellTypeBox.getSelectedItem().toString().toUpperCase());
            }
            else {
                stm.insert(propertyIdField.getText(), buyerIdField.getText(), 
                        staffIdField.getText(), datePicker.getEditor().getText(), 
                        reservedField.getText(),sellingField.getText(),
                        depositField.getText(),balanceField.getText(), 
                        sellTypeBox.getSelectedItem().toString().toUpperCase());
            }
            parentWindow.updateTableData();
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }//GEN-LAST:event_saveButtonMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField balanceField;
    private javax.swing.JTextField buyerIdField;
    private javax.swing.JButton cancelButton;
    private org.jdesktop.swingx.JXDatePicker datePicker;
    private javax.swing.JTextField depositField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField propertyIdField;
    private javax.swing.JTextField reservedField;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox sellTypeBox;
    private javax.swing.JTextField sellingField;
    private javax.swing.JTextField staffIdField;
    // End of variables declaration//GEN-END:variables
}
